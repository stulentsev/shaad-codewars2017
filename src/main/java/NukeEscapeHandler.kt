object NukeEscapeHandler {
    private var lastOpponentTickIndex = -1
    private var alreadyRegrouping = false
    fun escapeFromNuke() {
        val nextNTickIndex = State.world.opponentPlayer.nextNuclearStrikeTickIndex
        if (nextNTickIndex == -1 && lastOpponentTickIndex != -1) {
            alreadyRegrouping = false
        }
        if (nextNTickIndex == -1 || alreadyRegrouping) {
            return
        }
        lastOpponentTickIndex = nextNTickIndex
        alreadyRegrouping = true

        val nextNX = State.world.opponentPlayer.nextNuclearStrikeX
        val nextNY = State.world.opponentPlayer.nextNuclearStrikeY
        GroupManager.idToGroup.filter {
            val group = it.value

            group.unitsIds()
                    .map { Vehicles.vehicle(it) }
                    .any { v ->
                        Coordinates.distance(v, nextNX, nextNY) < State.game.tacticalNuclearStrikeRadius
                    }
        }.forEach {
            val group = it.value
            val groupId = it.key

            Moves.addGroupSelection(groupId)
            Moves.addScale(nextNX, nextNY, 7.0, {
                val endOfNukeTick = lastOpponentTickIndex - State.world.tickIndex
                val nextNukeIndex = lastOpponentTickIndex
                RegroupManager.addRemainingTicks(group.id, endOfNukeTick)
                Moves.addActionsOnCondition(
                        { State.world.tickIndex >= nextNukeIndex },
                        {
                            Moves.addGroupSelection(groupId)
                            Moves.addScale(nextNX, nextNY, 0.1, {
                                RegroupManager.addRemainingTicks(group.id, 40)
                            })
                        })
            })
        }
    }

}