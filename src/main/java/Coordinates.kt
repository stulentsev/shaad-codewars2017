import model.Unit
import model.Vehicle

object Coordinates {
    fun box(units: Collection<Unit>): Box {
        val maxX = units.map(Unit::x).max() ?: 0.0
        val minX = units.map(Unit::x).min() ?: 0.0
        val maxY = units.map(Unit::y).max() ?: 0.0
        val minY = units.map(Unit::y).min() ?: 0.0
        return Box(minX - 1, minY - 1, maxX + 1, maxY + 1)
    }

    fun box(group: Group): Box {
        return box(Vehicles.myVehicles(group).toList())
    }

    fun distance(x1: Double, y1: Double, x2: Double, y2: Double): Double {
        return FastMath.hypot(x2 - x1, y2 - y1)
    }

    fun distance(vehicle: Vehicle, x2: Double, y2: Double): Double {
        return FastMath.hypot(x2 - vehicle.x, y2 - vehicle.y)
    }

}
