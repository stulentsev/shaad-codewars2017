import model.VehicleType

class FighterMoveCalculator : MoveCalculatorBase() {
    override fun computeScoreForCoordinates(box: Box, group: Group, x: Double, y: Double): Double {
        val targetScore =
                if (shouldRepair(group))
                    Vehicles.myVehicles(VehicleType.ARRV)
                            .map { v -> 100 / safeDistanceTo(x, y, v) }.sum()
                else
                    max(getTargetScores(x, y, VehicleType.HELICOPTER, 100),
                            getTargetScores(x, y, VehicleType.FIGHTER, 50))

        val counterUnitsScore = getDangerScore(group, x, y)

        return modifyByAllyCollisions(targetScore - counterUnitsScore, x, y, group, true)
    }
}

