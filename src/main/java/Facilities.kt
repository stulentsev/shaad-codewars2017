import model.Facility

object Facilities {
    fun facilities(ownership: Ownership = Ownership.ANY): Sequence<Facility> {
        var sequence = State.world.facilities.asSequence()

        sequence = when (ownership) {
            Ownership.ANY -> sequence
            Ownership.MY -> sequence.filter { State.world.myPlayer.id == it.ownerPlayerId }
            Ownership.ENEMY -> sequence.filter {
                State.world.myPlayer.id != it.ownerPlayerId
                        && it.ownerPlayerId != -1L
            }
        }
        return sequence
    }

    fun notMineFacilities(): Sequence<Facility> {
        return State.world.facilities.asSequence()
                .filter { it.ownerPlayerId != State.world.myPlayer.id }
    }

    fun mineFacilities(): Sequence<Facility> {
        return facilities(Ownership.MY)
    }
}