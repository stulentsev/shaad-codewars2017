import model.ActionType
import model.Move
import model.VehicleType
import java.util.*

object Moves {
    private val moves = ArrayDeque<(Move) -> Unit>()
    private val actionsOnCondition = mutableListOf<Pair<() -> Boolean, () -> Unit>>()

    fun add(move: (Move) -> Unit) {
        moves.add(move)
    }

    fun addActionsOnCondition(condition: () -> Boolean, move: () -> Unit) {
        actionsOnCondition.add(condition to move)
    }

    fun checkForConditionMoves() {
        val actions = actionsOnCondition.filter { it.first() }
        actionsOnCondition.removeAll(actions)
        actions.forEach { it.second() }
    }

    fun addBoxSelection(box: Box, vehicleType: VehicleType? = null, action: () -> Unit) {
        add { move ->
            move.left = box.minX
            move.top = box.minY
            move.right = box.maxX
            move.bottom = box.maxY
            move.action = ActionType.CLEAR_AND_SELECT
            if (vehicleType != null) {
                move.vehicleType = vehicleType
            }
            action()
        }
    }

    fun addAssign(groupId: Int) {
        add { m ->
            m.action = ActionType.ASSIGN
            m.group = groupId
        }
    }

    fun addGroupSelection(groupId: Int) {
        add { move ->
            move.group = groupId
            move.action = ActionType.CLEAR_AND_SELECT
        }
    }

    fun addMoveToPoint(box: Box, targetX: Double, targetY: Double, maxSpeed: Double) {
        addMoveToPoint(box.middleX, box.middleY, targetX, targetY, maxSpeed)
    }


    fun addMoveToPoint(currentX: Double, currentY: Double,
                       targetX: Double, targetY: Double,
                       maxSpeed: Double? = null) {
        add { move ->
            move.x = targetX - currentX
            move.y = targetY - currentY
            move.action = ActionType.MOVE
            if (maxSpeed != null) {
                move.maxSpeed = maxSpeed
            }
        }
    }

    fun addVehicleProduction(vehicleType: VehicleType, facilityId: Long) {
        add { move ->
            move.action = ActionType.SETUP_VEHICLE_PRODUCTION
            move.facilityId = facilityId
            move.vehicleType = vehicleType
        }
    }

    fun addScale(x: Double, y: Double, factor: Double, action: () -> Unit = {}) {
        add { move ->
            action()
            move.action = ActionType.SCALE
            move.x = x
            move.y = y
            move.factor = factor
        }
    }

    fun clearMoves() {
        moves.clear()
    }

    fun queuedMovesCount(): Int {
        return moves.size
    }

    fun executeNextMove() {
        val delayedMove = moves.poll() ?: return

        delayedMove(State.move)
    }
}
