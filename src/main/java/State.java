import model.*;

import java.util.HashMap;
import java.util.Map;
import java.util.Random;

class State {
    static Player me;
    static World world;
    static Game game;
    static Move move;

    static final Map<Long, Integer> updateTickByVehicleId = new HashMap<>();

    static Random random;
    static TerrainType[][] terrainTypeByCellXY;
    static WeatherType[][] weatherTypeByCellXY;

    static void initializeStrategy(World world, Game game) {
        if (State.random == null) {
            State.random = new Random(game.getRandomSeed());

            State.terrainTypeByCellXY = world.getTerrainByCellXY();
            State.weatherTypeByCellXY = world.getWeatherByCellXY();
        }
        if (State.game == null) {
            State.game = game;
        }
    }

    static void initializeTick(Player me, World world, Game game, Move move) {
        State.me = me;
        State.world = world;
        State.game = game;
        State.move = move;

        for (Vehicle vehicle : world.getNewVehicles()) {
            Vehicles.INSTANCE.getVehicleById().put(vehicle.getId(), vehicle);
            updateTickByVehicleId.put(vehicle.getId(), world.getTickIndex());
        }

        for (VehicleUpdate vehicleUpdate : world.getVehicleUpdates()) {
            long vehicleId = vehicleUpdate.getId();

            if (vehicleUpdate.getDurability() == 0) {
                Vehicles.INSTANCE.getVehicleById().remove(vehicleId);
                updateTickByVehicleId.remove(vehicleId);
                GroupManager.INSTANCE.removeDeadVehicle(vehicleId);
            } else {
                Vehicles.INSTANCE.getVehicleById().put(vehicleId,
                        new Vehicle(Vehicles.INSTANCE.getVehicleById().get(vehicleId), vehicleUpdate));
                updateTickByVehicleId.put(vehicleId, world.getTickIndex());
            }
        }
        Vehicles.INSTANCE.updateVehicles();
        VehicleQuadTree.INSTANCE.update();
    }
}
