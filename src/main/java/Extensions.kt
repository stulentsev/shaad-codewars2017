import model.TerrainType
import model.Vehicle
import model.VehicleType
import model.WeatherType

fun Vehicle.ownership() =
        when (this.playerId == State.world!!.myPlayer.id) {
            true -> Ownership.MY
            else -> Ownership.ENEMY
        }

fun VehicleType.isAerial() =
        when (this) {
            VehicleType.FIGHTER -> true
            VehicleType.HELICOPTER -> true
            else -> false
        }

fun VehicleType.speed() =
        when (this) {
            VehicleType.FIGHTER -> State.game!!.fighterSpeed
            VehicleType.HELICOPTER -> State.game!!.helicopterSpeed
            VehicleType.TANK -> State.game!!.tankSpeed
            VehicleType.IFV -> State.game!!.ifvSpeed
            VehicleType.ARRV -> State.game!!.arrvSpeed
        }

fun VehicleType.productionCost() =
        when (this) {
            VehicleType.FIGHTER -> State.game!!.fighterProductionCost
            VehicleType.HELICOPTER -> State.game!!.helicopterProductionCost
            VehicleType.TANK -> State.game!!.tankProductionCost
            VehicleType.IFV -> State.game!!.ifvProductionCost
            VehicleType.ARRV -> State.game!!.arrvProductionCost
        }

fun WeatherType.speedReduceMultiplier() =
        when (this) {
            WeatherType.CLEAR -> State.game!!.clearWeatherSpeedFactor
            WeatherType.RAIN -> State.game!!.rainWeatherSpeedFactor
            else -> State.game!!.cloudWeatherSpeedFactor
        }

fun TerrainType.speedReduceMultiplier() =
        when (this) {
            TerrainType.FOREST -> State.game!!.forestTerrainSpeedFactor
            TerrainType.SWAMP -> State.game!!.swampTerrainSpeedFactor
            else -> State.game!!.plainTerrainSpeedFactor
        }
