object Constants {
    val GRID_STEP = 15
    val NUKE_GRID_STEP = 30
    val MAX_MAP_BORDER_PENALTY = 10000
    val MIN_MAP_BORDER_PENALTY = 5000
    val MOVE_FREQUENCY = 25
}
