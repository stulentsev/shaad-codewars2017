import model.VehicleType

class IfvMoveCalculator : MoveCalculatorBase() {
    override fun computeScoreForCoordinates(box: Box, group: Group, x: Double, y: Double): Double {
        //first step - count target units score
        val targetScore = max(getTargetScores(x, y, VehicleType.HELICOPTER, 200),
                getTargetScores(x, y, VehicleType.FIGHTER, 200),
                getTargetScores(x, y, VehicleType.IFV, 150),
                getTargetScores(x, y, VehicleType.TANK, 100),
                getTargetScores(x, y, VehicleType.ARRV, 200),
                getFacilityScore(group, x, y, 20000))

        //second step - count units which counter current unit
        val counterUnitsScore = getDangerScore(group, x, y)

        return modifyByAllyCollisions(targetScore - counterUnitsScore, x, y, group)
    }
}

