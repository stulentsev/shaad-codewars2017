import model.VehicleType

class HelicopterMoveCalculator : MoveCalculatorBase() {
    override fun computeScoreForCoordinates(box: Box, group: Group, x: Double, y: Double): Double {
        val shouldRepair = shouldRepair(group)
        val targetScore =
                when {
                    shouldRepair -> Vehicles.myVehicles(VehicleType.ARRV).map { v -> 100 / safeDistanceTo(x, y, v) }.sum()
                    Vehicles.enemyVehicles(VehicleType.FIGHTER).count() > 20
                            && Vehicles.myVehicles(VehicleType.IFV).count() > 30
                            && State.world.tickIndex < 10000 -> getAllyIfvScore(x, y)
                    else -> max(getTargetScores(x, y, VehicleType.TANK, 350),
                            getTargetScores(x, y, VehicleType.ARRV, 300),
                            getTargetScores(x, y, VehicleType.HELICOPTER, 300),
                            getTargetScores(x, y, VehicleType.FIGHTER, 100),
                            getTargetScores(x, y, VehicleType.IFV, 200))
                }


        //second step - count units which counter current unit
        var counterUnitsScore = getDangerScore(group, x, y)

        if (shouldRepair) {
            counterUnitsScore *= 100
        }

        return modifyByAllyCollisions(targetScore - counterUnitsScore, x, y, group, true)
    }

    private fun getAllyIfvScore(x: Double, y: Double) = Vehicles.myVehicles(VehicleType.IFV).map { v -> rewardByDistance(safeDistanceTo(x, y, v), 10000) }.sum()
}

