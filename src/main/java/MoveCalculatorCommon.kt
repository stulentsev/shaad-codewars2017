import model.VehicleType
import java.util.*

class MoveCalculatorCommon : MoveCalculator {
    private val vehicleType2Calculator = EnumMap<VehicleType, MoveCalculator>(VehicleType::class.java)

    init {
        vehicleType2Calculator.put(VehicleType.FIGHTER, FighterMoveCalculator())
        vehicleType2Calculator.put(VehicleType.HELICOPTER, HelicopterMoveCalculator())
        vehicleType2Calculator.put(VehicleType.TANK, TankMoveCalculator())
        vehicleType2Calculator.put(VehicleType.IFV, IfvMoveCalculator())
        vehicleType2Calculator.put(VehicleType.ARRV, ArrvMoveCalculator())
    }

    override fun calculateFieldForUnits(vehicleType: VehicleType, box: Box, group: Group): PFieldNode {
        val moveCalculator = vehicleType2Calculator[vehicleType]
        return moveCalculator?.calculateFieldForUnits(vehicleType, box, group)
                ?: PFieldNode(box.middleX, box.minY, 0.0)
    }

    override fun getLastField(vehicleType: VehicleType, groupId: Int): List<PFieldNode>? {
        val moveCalculator = vehicleType2Calculator[vehicleType]
        return moveCalculator?.getLastField(vehicleType, groupId)
                ?: listOf()

    }
}

